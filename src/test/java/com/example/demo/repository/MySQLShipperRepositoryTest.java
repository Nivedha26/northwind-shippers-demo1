/**
 * 
 */
package com.example.demo.repository;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;

import com.example.demo.entities.Shipper;

@ActiveProfiles("h2")
@SpringBootTest
public class MySQLShipperRepositoryTest {

	@Autowired
	MySQLShipperRepository mySQLShipperRepository;
		
	@Test
	public void testGetAllShippers() {
		// 1. Any setup stuff
		
		// 2. Call the method under test
		List<Shipper> returnedList = mySQLShipperRepository.getAllShippers();
		
		// 3 Verify the results
		assertThat(returnedList).isNotEmpty();
		
		for(Shipper shipper: returnedList) {
			System.out.println("shipper name: " + shipper.getName());
		}
	}
}
